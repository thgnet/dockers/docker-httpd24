#!/bin/bash

set -eu

pkg="${1:-}"
ver="${2:-}"

if [ -z "$pkg" -o -z "$ver" ]
then
  echo "Usage: $0 <package> <version>" >&2
  exit 1
fi

case "$pkg" in
  nghttp2)
    url_dist="https://github.com/nghttp2/nghttp2/releases/download/v$ver/nghttp2-$ver.tar.gz"
    url_sign="https://github.com/nghttp2/nghttp2/releases/download/v$ver/nghttp2-$ver.tar.gz.asc"
    (
      cd builder/builder-files/archives
      rm -f $pkg-*.tar.gz*
      wget "$url_dist" "$url_sign"
      gpg --verify $pkg-$ver.tar.gz.asc
      date --utc +"%Y-%m-%dT%H:%M:%SZ" -r $pkg-$ver.tar.gz > $pkg-$ver.tar.gz.stamp
    )
    sed -i 's/\(BUILDER_NGHTTP2_VERSION\)=".*"/\1="'$ver'"/' Dockerfile.tpl.sh
    ;;

  httpd)
    url_dist="https://dlcdn.apache.org/httpd/httpd-$ver.tar.gz"
    url_sign="https://downloads.apache.org/httpd/httpd-$ver.tar.gz.asc"
    (
      cd builder/builder-files/archives
      rm -f $pkg-*.tar.gz*
      wget "$url_dist" "$url_sign"
      gpg --verify $pkg-$ver.tar.gz.asc || :
      date --utc +"%Y-%m-%dT%H:%M:%SZ" -r $pkg-$ver.tar.gz > $pkg-$ver.tar.gz.stamp
    )
    sed -i 's/\(BUILDER_HTTPD_VERSION\)=".*"/\1="'$ver'"/' Dockerfile.tpl.sh
    ;;

esac

echo
echo "SUCCESS"
echo
