#!/bin/bash

set -eu

namevx="httpd24"

default_file="${namevx}-build-$(arch).tar.gz"
[ -e "$default_file" ] || default_file=""

opt_hexdump=""
if [ "${1:-}" = "-x" ]
then
  opt_hexdump="1"
  shift
fi

xdiff_file="${1:-$default_file}"
if [ -z "$xdiff_file" -o "$xdiff_file" = "-h" ]
then
  echo "Usage: $0 [-x] <file.tar.gz> [file-base.tar.gz]" >&2
  exit 1
fi

if [ ! -e "$xdiff_file" ]
then
  echo "Error: Cannot locate file \"$xdiff_file\"" >&2
  exit 1
fi

xdiff_base="${2:-}"
if [ -z "$xdiff_base" ]
then
  xdiff_md5_1=$(git show :$xdiff_file | md5sum - | awk '{ print $1 }')
  xdiff_label_1="$xdiff_file (git)"
  xdiff_md5_2=$(cat "$xdiff_file" | md5sum - | awk '{ print $1 }')
  xdiff_label_2="$xdiff_file (wc)"
else
  if [ ! -e "$xdiff_base" ]
  then
    echo "Error: Cannot locate base file \"$xdiff_base\"" >&2
    exit 1
  fi
  xdiff_md5_1=$(cat "$xdiff_base" | md5sum - | awk '{ print $1 }')
  xdiff_label_1="$xdiff_base"
  xdiff_md5_2=$(cat "$xdiff_file" | md5sum - | awk '{ print $1 }')
  xdiff_label_2="$xdiff_file"
fi
echo ""
echo "Comparing files:"
echo "    HEAD: $xdiff_md5_1  $xdiff_label_1"
echo "    WORK: $xdiff_md5_2  $xdiff_label_2"
echo ""

if [ "$xdiff_md5_1" = "$xdiff_md5_2" ]
then
  echo "Files are identical"
  exit 0
fi

rm -rf xdiff_1 xdiff_2
mkdir -p xdiff_1 xdiff_2
if [ -z "$xdiff_base" ]
then
  git show :"$xdiff_file" | tar -C xdiff_1 -zx
else
  cat "$xdiff_base" | tar -C xdiff_1 -zx
fi
cat "$xdiff_file" | tar -C xdiff_2 -zx

fixup_php() {
  # fixup pear reg
  for ii in $(find $1 -name "*.reg")
  do
    cat $ii | php -r 'var_export(unserialize(stream_get_contents(STDIN)));' > $ii.var
    rm $ii
  done

  # fixup pear conf
  for ii in $(find $1 -name "pear.conf")
  do
    tail -n +2 $ii | php -r 'var_export(unserialize(stream_get_contents(STDIN)));' > $ii.var
    rm $ii
  done

  # add strings to binary files to help investigate
  for ii in $(find $1 -name "*.so") $1/usr/local/bin/php
  do
    if [ -n "$opt_hexdump" ]
    then
      hexdump -C $ii > $ii.hex
    else
      strings -6 $ii > $ii.strings
    fi
  done
}

fixup_mysql() {
  for ii in $(file $1/usr/local/mysql/bin/* | grep 'ELF' | cut -d: -f1) $(find . -name "*.a")
  do
    if [ -n "$opt_hexdump" ]
    then
      hexdump -C $ii > $ii.hex
    else
      strings -6 $ii > $ii.strings
    fi
  done
}

if [[ $xdiff_file =~ ^php.*-build.* ]]
then
  echo "Applying PHP bundle fixups"
  fixup_php xdiff_1
  fixup_php xdiff_2
fi

if [[ $xdiff_file =~ ^mysql.*-build.* ]]
then
  echo "Applying MySQL bundle fixups"
  fixup_mysql xdiff_1
  fixup_mysql xdiff_2
fi

diff -ur --no-dereference xdiff_1 xdiff_2
