#!/bin/bash
#
# builder/builder-files/bin/build-httpd24.sh
#
# Builds Apache HTTPd and generates a binary install package in:
#    /srv/builder/products/httpd24-build.tar.gz
#

set -eu

out() {
  echo
  echo
  echo "[+++] $1"
  echo
  echo
}

filesdir="/srv/builder/files"
workdir="/srv/builder/runtime"
outdir="/srv/builder/products"

srcpkg="httpd"
srcname="$srcpkg-$BUILDER_HTTPD_VERSION"
srcfile="$srcname.tar.gz"
srcdir="$workdir/$srcname"

outfile="httpd24-build.tar.gz"

cd $workdir

verbose=""
srcstamp=$(cat $filesdir/archives/$srcfile.stamp)
export SOURCE_DATE_EPOCH=$(date --utc --date="$srcstamp" +%s)

if [ "${1:-}" = "-patch" ]
then
  # To create a patch, the runtime state must be a clean "unpack"
  if [ -e .stamp.$srcpkg._1configure -o ! -e .stamp.$srcpkg._0unpack ]
  then
    echo "Error: Invalid runtime state, must be unpack" >&2
    exit 1
  fi
  out "Updating source patch file..."
  diff -ur $srcname-orig $srcname | \
    sed -e '/^diff /d; s/^\([+-]\+ .*\)\t[0-9:.+ -]\+/\1/' > $srcname.patch
  if cmp -s $srcname.patch $filesdir/archives/$srcname.patch
  then
    echo "No need to update the patch file."
  else
    cat $srcname.patch > $filesdir/archives/$srcname.patch
    echo "Patch file updated."
  fi
  rm -f $srcname.patch
  out "Done."
  exit
fi

if [ "${1:-}" = "-verbose" ]
then
  verbose="1"
  shift
fi

##############################################################################

do_depends() {
  out "Building and installing dependencies..."

  # We run the package action (which does nothing) just to trigger the full
  # sequence only if needed, otherwise it does nothing
  $filesdir/bin/build-openssl.sh package
  $filesdir/bin/build-nghttp2.sh package
  $filesdir/bin/build-apr17.sh package
  $filesdir/bin/build-apru16.sh package
}

##############################################################################

do_unpack() {
  out "Unpacking source file '$srcfile'..."
  rm -rf $srcname $srcname-orig
  tar -zx --no-same-owner -f $filesdir/archives/$srcfile
  cp -a $srcname $srcname-orig

  # Apply local patches
  if [ -e "$filesdir/archives/$srcname.patch" ]
  then
    echo "Applying local patch '$srcname.patch'"
    (cd $srcname; patch -p1 < $filesdir/archives/$srcname.patch)
  fi

  touch .stamp.$srcpkg._0unpack
  rm -f .stamp.$srcpkg._1configure
  rm -f .stamp.$srcpkg._2build
  rm -f .stamp.$srcpkg._3install
}

##############################################################################

do_configure() {
  [ -e .stamp.$srcpkg._0unpack ] || do_unpack

  out "Starting configure script inside '$srcname'..."
  pushd $srcdir >/dev/null
  (set -x;
    ./configure \
        --sysconfdir=/srv/config/apache2 \
        --enable-http2 \
        --enable-mpms-shared=all \
        --enable-suexec \
        --with-suexec-caller=httpd \
        --with-suexec-docroot=/srv/webroots \
        --with-suexec-uidmin=1000 \
        --with-suexec-gidmin=1000 \
        --with-suexec-logfile=/srv/logs/default/suexec_log \
  )
  popd >/dev/null

  touch .stamp.$srcpkg._1configure
  rm -f .stamp.$srcpkg._2build
  rm -f .stamp.$srcpkg._3install
}

##############################################################################

do_build() {
  [ -e .stamp.$srcpkg._1configure ] || do_configure

  pushd $srcdir >/dev/null
  if [ -n "$verbose" ]
  then
    out "Starting compilation with single processor..."
    make
  else
    local cpus=`grep -c ^processor /proc/cpuinfo`
    out "Starting compilation with $cpus processor/s..."
    make -j $cpus
  fi
  popd >/dev/null

  touch .stamp.$srcpkg._2build
  rm -f .stamp.$srcpkg._3install
}

##############################################################################

_do_install_clean() {
  out "Cleaning install files..."
  rm -rf /usr/local/apache2
  rm -f .stamp.$srcpkg._3install
}

do_install() {
  [ -e .stamp.$srcpkg._2build ] || do_build

  _do_install_clean

  pushd $srcdir >/dev/null
  out "Installing..."

  # First, we install on a rebased prefix to trick away the config data
  rm -rf /usr/local/apache2-instroot
  make install DESTDIR=/usr/local/apache2-instroot

  # Now we relocate the files where they belong to
  rm -rf /usr/local/apache2
  mv /usr/local/apache2-instroot/usr/local/apache2 \
      /usr/local/apache2
  mv /usr/local/apache2-instroot/srv/config/apache2 \
      /usr/local/apache2/config.dist
  rm -rf /usr/local/apache2-instroot

  # Strip binaries to save (a lot of) space
  (
    cd /usr/local/apache2/bin
    file * | grep ' ELF ' | cut -d: -f1 | xargs strip
  )

  # Remove unwanted stuff
  rm -rf /usr/local/apache2/build
  rm -rf /usr/local/apache2/cgi-bin/*
  rm -rf /usr/local/apache2/config.dist/original
  rm -rf /usr/local/apache2/include
  rm -rf /usr/local/apache2/logs
  rm -rf /usr/local/apache2/man
  rm -rf /usr/local/apache2/manual

  popd >/dev/null

  touch .stamp.$srcpkg._3install
}

##############################################################################

do_package() {
  [ -e .stamp.$srcpkg._3install ] || do_install

  out "Packaging and publishing..."
  rm -f /tmp/$outfile /tmp/$outfile.list /tmp/$outfile.manifest
  (cd /;
    find \
      usr/local/lib/*.so.* \
      usr/local/lib/engines-3 \
      usr/local/apr/lib/*.so.* \
      usr/local/apache2 \
      -print0 | \
    LC_ALL=C sort -z | \
    tar --no-recursion --null -T - -c --numeric-owner \
      --owner=0 --group=0 --mtime=@$SOURCE_DATE_EPOCH | \
    gzip -9nf > /tmp/$outfile)
  (cd /tmp; md5sum $outfile)
  tar -tf /tmp/$outfile > /tmp/$outfile.manifest
  tar -tvf /tmp/$outfile > /tmp/$outfile.list
  mv /tmp/$outfile /tmp/$outfile.list /tmp/$outfile.manifest $outdir
}

##############################################################################

do_clean() {
  _do_install_clean

  out "Cleaning runtime information..."
  rm -rf $srcname $srcname-orig .stamp.$srcpkg.*
}

##############################################################################

do_help() {
  echo "Usage: $0 <target>" >&2
  echo "Targets:" >&2
  echo " - depends (d)" >&2
  echo " - unpack (u)" >&2
  echo " - configure (c)" >&2
  echo " - build (b)" >&2
  echo " - install (i)" >&2
  echo " - package (p)" >&2
  echo " - all (a)" >&2
}

##############################################################################

for action in ${@:-help}
do
  case "$action" in
    h|help)
      do_help
      ;;
    d|depends)
      do_depends
      ;;
    u|unpack)
      do_unpack
      ;;
    c|conf|configure)
      do_configure
      ;;
    b|build)
      do_build
      ;;
    i|install)
      do_install
      ;;
    p|pkg|package)
      do_package
      ;;
    x|clean)
      do_clean
      ;;
    xi|clean-install)
      _do_install_clean
      ;;
    a|all)
      do_depends
      do_clean
      do_unpack
      do_configure
      do_build
      do_install
      do_package
      ;;
    *)
      echo "Error: Unknown action \"$action\"" >&2
      exit 1
      ;;
  esac
done
