#!/bin/bash
#
# builder/builder-files/bin/build-nghttp2.sh
#
# Builds the nghttp2 library.
#

set -eu

out() {
  echo
  echo
  echo "[+++] $1"
  echo
  echo
}

filesdir="/srv/builder/files"
workdir="/srv/builder/runtime"
outdir="/srv/builder/products"

srcpkg="nghttp2"
srcname="$srcpkg-$BUILDER_NGHTTP2_VERSION"
srcfile="$srcname.tar.gz"
srcdir="$workdir/$srcname"

cd $workdir

verbose=""
srcstamp=$(cat $filesdir/archives/$srcfile.stamp)
export SOURCE_DATE_EPOCH=$(date --utc --date="$srcstamp" +%s)

if [ "${1:-}" = "-patch" ]
then
  # To create a patch, the runtime state must be a clean "unpack"
  if [ -e .stamp.$srcpkg._1configure -o ! -e .stamp.$srcpkg._0unpack ]
  then
    echo "Error: Invalid runtime state, must be unpack" >&2
    exit 1
  fi
  out "Updating source patch file..."
  diff -ur $srcname-orig $srcname | \
    sed -e '/^diff /d; s/^\([+-]\+ .*\)\t[0-9:.+ -]\+/\1/' > $srcname.patch
  if cmp -s $srcname.patch $filesdir/archives/$srcname.patch
  then
    echo "No need to update the patch file."
  else
    cat $srcname.patch > $filesdir/archives/$srcname.patch
    echo "Patch file updated."
  fi
  rm -f $srcname.patch
  out "Done."
  exit
fi

if [ "${1:-}" = "-verbose" ]
then
  verbose="1"
  shift
fi

##############################################################################

do_unpack() {
  out "Unpacking source file '$srcfile'..."
  rm -rf $srcname $srcname-orig
  tar -zx --no-same-owner -f $filesdir/archives/$srcfile
  cp -a $srcname $srcname-orig

  # Apply local patches
  if [ -e "$filesdir/archives/$srcname.patch" ]
  then
    echo "Applying local patch '$srcname.patch'"
    (cd $srcname; patch -p1 < $filesdir/archives/$srcname.patch)
  fi

  touch .stamp.$srcpkg._0unpack
  rm -f .stamp.$srcpkg._1configure
  rm -f .stamp.$srcpkg._2build
  rm -f .stamp.$srcpkg._3install
}

##############################################################################

do_configure() {
  [ -e .stamp.$srcpkg._0unpack ] || do_unpack

  out "Starting configure script inside '$srcname'..."
  pushd $srcdir >/dev/null
  (set -x;
    ./configure \
  )
  popd >/dev/null

  touch .stamp.$srcpkg._1configure
  rm -f .stamp.$srcpkg._2build
  rm -f .stamp.$srcpkg._3install
}

##############################################################################

do_build() {
  [ -e .stamp.$srcpkg._1configure ] || do_configure

  pushd $srcdir >/dev/null
  if [ -n "$verbose" ]
  then
    out "Starting compilation with single processor..."
    make
  else
    local cpus=`grep -c ^processor /proc/cpuinfo`
    out "Starting compilation with $cpus processor/s..."
    make -j $cpus
  fi
  popd >/dev/null

  touch .stamp.$srcpkg._2build
  rm -f .stamp.$srcpkg._3install
}

##############################################################################

_do_install_clean() {
  out "Cleaning install files..."
  rm -rf /usr/local/bin/deflatehd
  rm -rf /usr/local/bin/inflatehd
  rm -rf /usr/local/include/nghttp2
  rm -rf /usr/local/lib/libnghttp2.*
  rm -rf /usr/local/lib/pkgconfig/libnghttp2.pc
  #rm -rf /usr/local/share/doc
  #rm -rf /usr/local/share/man/man*/*
  #rm -rf /usr/local/share/nghttp2
  rm -f .stamp.$srcpkg._3install
}

do_install() {
  [ -e .stamp.$srcpkg._2build ] || do_build

  _do_install_clean

  pushd $srcdir >/dev/null
  out "Installing..."
  make install
  # Remove unwanted stuff
  rm -rf /usr/local/share/doc
  rm -rf /usr/local/share/man/man*/*
  rm -rf /usr/local/share/nghttp2
  popd >/dev/null

  touch .stamp.$srcpkg._3install
}

##############################################################################

do_package() {
  [ -e .stamp.$srcpkg._3install ] || do_install

  out "No packaging required"
}

##############################################################################

do_clean() {
  _do_install_clean

  out "Cleaning runtime information..."
  rm -rf $srcname $srcname-orig .stamp.$srcpkg.*
}

##############################################################################

do_help() {
  echo "Usage: $0 <target>" >&2
  echo "Targets:" >&2
  echo " - unpack (u)" >&2
  echo " - configure (c)" >&2
  echo " - build (b)" >&2
  echo " - install (i)" >&2
  echo " - package (p)" >&2
  echo " - all (a)" >&2
}

##############################################################################

for action in ${@:-help}
do
  case "$action" in
    h|help)
      do_help
      ;;
    u|unpack)
      do_unpack
      ;;
    c|conf|configure)
      do_configure
      ;;
    b|build)
      do_build
      ;;
    i|install)
      do_install
      ;;
    p|pkg|package)
      do_package
      ;;
    x|clean)
      do_clean
      ;;
    xi|clean-install)
      _do_install_clean
      ;;
    a|all)
      do_clean
      do_unpack
      do_configure
      do_build
      do_install
      do_package
      ;;
    *)
      echo "Error: Unknown action \"$action\"" >&2
      exit 1
      ;;
  esac
done
