#:TITLE:Apache HTTPd 2.4
#:VERSION:2
#:RELEASE:1
#:BUILD:2024-04-08T08:00:00Z
#:TAG:thgnet/httpd24:v@@VERSION@@.@@RELEASE@@
#:TAG:thgnet/httpd24:v@@VERSION@@
#:BUILDER_TAG:thgnet/httpd24builder:v@@VERSION@@
FROM thgnet/centos7:20240109

LABEL \
  org.label-schema.build-date="@@BUILD_DATE_ISO@@" \
  org.label-schema.name="@@TITLE@@" \
  org.label-schema.vendor="ThGnet" \
  org.label-schema.version="@@VERSION@@.@@RELEASE@@" \
  org.opencontainers.image.created="@@BUILD_DATE_OPEN@@" \
  org.opencontainers.image.title="@@TITLE@@" \
  org.opencontainers.image.vendor="ThGnet"

RUN set -eux; \
  # Add extended locale support
  sed -i '/^override_install_langs=/d' /etc/yum.conf; \
  yum -y reinstall glibc-common; \
  # Base environment
  yum -y install \
      pcre2 jansson; \
  # Funky stuff, no idea why this is not the default
  echo '/usr/local/lib' > /etc/ld.so.conf.d/local.conf; \
  ldconfig; \
  # Create system user early to set up the suexec
  groupadd -g 48 -r httpd; \
  useradd -u 48 -g 48 -r -d /srv httpd; \
  # Prepare the necessary runtime files
  echo '@@VERSION@@.@@RELEASE@@' > /opt/VERSION; \
  # Cleanup
  pwck -s; rm -f /etc/passwd- /etc/shadow-; \
  grpck -s; rm -f /etc/group- /etc/gshadow-; \
  yum clean all;

%%IF BUILDER -----------------------------------------------------------------
LABEL \
  org.label-schema.name="@@TITLE@@ Builder" \
  org.opencontainers.image.title="@@TITLE@@ Builder"

RUN set -eux; \
  # Build environment using devtoolset v9
  yum -y install centos-release-scl; \
  yum -y install \
    devtoolset-9 \
    # required by openssl
    perl-IPC-Cmd \
    # required by apr/apr-util
    expat-devel \
    # required by apache
    pcre2-devel \
    # required by httpd/mod_md
    libcurl-devel jansson-devel \
    # required by httpd/mod_deflate
    zlib-devel \
    # command line utilities
    file patch wget; \
  #
  # Build environment with system packages (doesn't work for nghttp2 > 1.18.1)
  #yum -y install \
  #  # basic compiler stuff
  #  gcc libtool \
  #  # for building openssl
  #  perl-IPC-Cmd \
  #  # for building nghttp2
  #  gcc-c++ \
  #  # utilities for build process management
  #  file patch \
  #  # general devel libraries required
  #  openssl-devel expat-devel pcre2-devel libcurl-devel jansson-devel; \
  #
  echo 'export PATH=$PATH:/srv/builder/files/bin' > /etc/profile.d/builder.sh; \
  echo 'alias bo=build-openssl.sh' >> /etc/profile.d/builder.sh; \
  echo 'alias bn=build-nghttp2.sh' >> /etc/profile.d/builder.sh; \
  echo 'alias ba=build-apr17.sh' >> /etc/profile.d/builder.sh; \
  echo 'alias bu=build-apru16.sh' >> /etc/profile.d/builder.sh; \
  echo 'alias bh=build-httpd24.sh' >> /etc/profile.d/builder.sh; \
  echo '. /opt/rh/devtoolset-9/enable' >> /etc/profile.d/builder.sh; \
  echo 'export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig${PKG_CONFIG_PATH:+:$PKG_CONFIG_PATH}' >> /etc/profile.d/builder.sh; \
  echo 'unset BASH_ENV' >> /etc/profile.d/builder.sh; \
  mkdir -p /usr/local/lib/pkgconfig; \
  mkdir -p /srv/builder; \
  mkdir -p /srv/builder/products; \
  mkdir -p /srv/builder/runtime;

ENV \
  BASH_ENV="/etc/profile.d/builder.sh" \
  BUILDER_OPENSSL_VERSION="3.2.1" \
  BUILDER_NGHTTP2_VERSION="1.61.0" \
  BUILDER_APR_VERSION="1.7.4" \
  BUILDER_APRU_VERSION="1.6.3" \
  BUILDER_HTTPD_VERSION="2.4.59" \
  SOURCE_DATE_EPOCH="@@BUILD_DATE_EPOCH@@"

COPY builder-files /srv/builder/files

WORKDIR /srv/builder/runtime

CMD [ "/srv/builder/files/bin/build-httpd24.sh", "all" ]
%%ENDIF BUILDER --------------------------------------------------------------
%%IF RUNTIME -----------------------------------------------------------------
ARG XARCH

ADD httpd24-build-$XARCH.tar.gz /

ADD files /

VOLUME /srv

EXPOSE 80 443

WORKDIR /srv

ENTRYPOINT [ "docker-entrypoint.sh" ]

CMD [ "/usr/local/apache2/bin/httpd", "-D", "FOREGROUND" ]
%%ENDIF RUNTIME --------------------------------------------------------------
