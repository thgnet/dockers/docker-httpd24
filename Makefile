
TITLE := $(shell sed -n -e 's/^\#:TITLE://p' Dockerfile.tpl.sh)
VERSION := $(shell sed -n -e 's/^\#:VERSION://p' Dockerfile.tpl.sh)
RELEASE := $(shell sed -n -e 's/^\#:RELEASE://p' Dockerfile.tpl.sh)
BUILD_DATE := $(shell sed -n -e 's/^\#:BUILD://p' Dockerfile.tpl.sh)
DOCKER_TAGS := $(shell sed -n -e 's/@@VERSION@@/$(VERSION)/; s/@@RELEASE@@/$(RELEASE)/; s/^\#:TAG://p' Dockerfile.tpl.sh)
DOCKER_BUILDER_TAGS := $(shell sed -n -e 's/@@VERSION@@/$(VERSION)/; s/@@RELEASE@@/$(RELEASE)/; s/^\#:BUILDER_TAG://p' Dockerfile.tpl.sh)

BUILD_DATE_ISO := $(BUILD_DATE)
BUILD_DATE_OPEN := $(shell date -u --date="$(BUILD_DATE)" +"%Y-%m-%d %H:%M:%S%:z")
BUILD_DATE_EPOCH := $(shell date -u --date="$(BUILD_DATE)" +"%s")

all: Dockerfile builder/Dockerfile

Dockerfile: Dockerfile.tpl.sh
	sed \
		-e 's/@@VERSION@@/$(VERSION)/' \
		-e 's/@@RELEASE@@/$(RELEASE)/' \
		-e 's/@@TITLE@@/$(TITLE)/' \
		-e 's/@@BUILD_DATE_ISO@@/$(BUILD_DATE_ISO)/' \
		-e 's/@@BUILD_DATE_OPEN@@/$(BUILD_DATE_OPEN)/' \
		-e '/^%%IF BUILDER/,/^%%ENDIF BUILDER/d' \
		-e '/^%%/d' \
		-e '/^\s*#/d' \
		Dockerfile.tpl.sh > $@

builder/Dockerfile: Dockerfile.tpl.sh
	sed \
		-e 's/@@VERSION@@/$(VERSION)/' \
		-e 's/@@RELEASE@@/$(RELEASE)/' \
		-e 's/@@TITLE@@/$(TITLE)/' \
		-e 's/@@BUILD_DATE_ISO@@/$(BUILD_DATE_ISO)/' \
		-e 's/@@BUILD_DATE_OPEN@@/$(BUILD_DATE_OPEN)/' \
		-e 's/@@BUILD_DATE_EPOCH@@/$(BUILD_DATE_EPOCH)/' \
		-e '/^%%IF RUNTIME/,/^%%ENDIF RUNTIME/d' \
		-e '/^%%/d' \
		-e '/^\s*#/d' \
		Dockerfile.tpl.sh > $@

.stamp.Dockerfile: Dockerfile $(shell find files -not -type l) httpd24-build-$(shell arch).tar.gz
	docker build --build-arg XARCH="$(shell arch)" $(addprefix -t ,$(DOCKER_TAGS)) .
	echo '$(DOCKER_TAGS)' > $@

builder/.stamp.Dockerfile: builder/Dockerfile $(shell find builder/builder-files -not -type l)
	(cd builder; docker build $(addprefix -t ,$(DOCKER_BUILDER_TAGS)) .)
	echo '$(DOCKER_BUILDER_TAGS)' > $@

build: .stamp.Dockerfile

builder: builder/.stamp.Dockerfile

push: .stamp.Dockerfile
	$(foreach DOCKER_TAG,$(DOCKER_TAGS),docker push $(DOCKER_TAG);)

clean:
	rm -f .stamp.Dockerfile builder/.stamp.Dockerfile
	rm -rf builder/builder-products builder/builder-runtime

.PHONY: all build builder clean
