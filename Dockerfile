FROM thgnet/centos7:20240109

LABEL \
  org.label-schema.build-date="2024-04-08T08:00:00Z" \
  org.label-schema.name="Apache HTTPd 2.4" \
  org.label-schema.vendor="ThGnet" \
  org.label-schema.version="2.1" \
  org.opencontainers.image.created="2024-04-08 08:00:00+00:00" \
  org.opencontainers.image.title="Apache HTTPd 2.4" \
  org.opencontainers.image.vendor="ThGnet"

RUN set -eux; \
  sed -i '/^override_install_langs=/d' /etc/yum.conf; \
  yum -y reinstall glibc-common; \
  yum -y install \
      pcre2 jansson; \
  echo '/usr/local/lib' > /etc/ld.so.conf.d/local.conf; \
  ldconfig; \
  groupadd -g 48 -r httpd; \
  useradd -u 48 -g 48 -r -d /srv httpd; \
  echo '2.1' > /opt/VERSION; \
  pwck -s; rm -f /etc/passwd- /etc/shadow-; \
  grpck -s; rm -f /etc/group- /etc/gshadow-; \
  yum clean all;

ARG XARCH

ADD httpd24-build-$XARCH.tar.gz /

ADD files /

VOLUME /srv

EXPOSE 80 443

WORKDIR /srv

ENTRYPOINT [ "docker-entrypoint.sh" ]

CMD [ "/usr/local/apache2/bin/httpd", "-D", "FOREGROUND" ]
