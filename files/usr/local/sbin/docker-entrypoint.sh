#!/bin/bash
set -eo pipefail
shopt -s nullglob

# This is required for unknown reasons
#ldconfig
export LD_LIBRARY_PATH=/usr/local/lib

export PATH=$PATH:/usr/local/apache2/bin

# If command starts with an option, prepend httpd
if [ "${1:0:1}" = '-' ]; then
	set -- httpd "$@"
fi

mkdir -p /srv/config

# Create a copy of the dist config files based on the container version
if [ ! -e /srv/config/apache2.dist-v$(cat /opt/VERSION) ]
then
  echo "[+] Creating apache2 dist config files data"
  cp -a /usr/local/apache2/config.dist \
      /srv/config/apache2.dist-v$(cat /opt/VERSION)
fi

# Create a copy of the config files if they don't exist
if [ ! -e /srv/config/apache2 ]
then
  echo "[+] Creating apache2 config files data"
  cp -a /usr/local/apache2/config.dist \
      /srv/config/apache2
fi

mkdir -p /srv/webroots
mkdir -p /srv/webroots/default

mkdir -p /srv/logs
mkdir -p /srv/logs/default

exec "$@"
